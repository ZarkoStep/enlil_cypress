/// <reference types="cypress" />

context('Enlil', () => {
    beforeEach(() => {
      cy.visit('https://demo.enlil.io')
    })
    it('Create a document', () => {
        cy.get('#LoginFormPresenter-email')
          .type('zarko.stepanov10@gmail.com')
          .should('have.value', 'zarko.stepanov10@gmail.com')
        cy.get('#LoginFormPresenter-password') 
          .type('Shubham##99')
        cy.get('#LoginFormPresenter-submit').click()  
        cy.get('#switchCompanyHeader')
          .contains('Switch Company')    
        cy.contains('Select a company below to sign-in to')
        //Click on company QA Zarko
        cy.get('button').contains('QA Zarko').click()
        
        //Document List
        cy.contains('Document List').should('have.attr', 'href','/document_revision').click()
        
        //Creating Document
        cy.get('#ListPresenter-create > .MuiButton-label').click()
        
        //Click Create a document
        //cy.get('.MuiButton-label').click()
        //TITILE 
        cy.get('#field-name > .MuiFormLabel-root > .MuiTypography-root').contains('h6','Title').should('be.visible')
        //Description
        cy.get('#field-description > .MuiFormLabel-root > .MuiTypography-root').contains('Description').should('be.visible')
        //cy.get('.DraftEditor-root').click().type('Hello')
        //Document type
        cy.contains('h6','Document Type').should('be.visible')
        
        //Document ID
        //cy.xpath('//div[4]/label[1]/h6[1]').contains('Document ID').should('be.visible')
        //cy.contains('Document ID').should('be.visible')
        
        //Document Revision Stage
        cy.contains('h6','Document Revision Stage').should('be.visible')
        //References

        //*Required fields
        cy.get('#field-name > .MuiFormHelperText-root').contains('Required').should('be.visible')
        cy.get('#field-description > .MuiFormHelperText-root').contains('Required').should('be.visible')
        
        cy.get('[name="name"]').should('be.visible').and('be.enabled')
        
        //cy.get('#mui-component-select-document.documentType.id').contains('Required').should('be.visible')

    })
})