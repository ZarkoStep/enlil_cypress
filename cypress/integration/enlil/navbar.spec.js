/// <reference types="cypress" />

context('Enlil', () => {
    beforeEach(() => {
      cy.visit('https://demo.enlil.io')
    })

//Navigation menu
it('Navigation menu', () => {
    cy.get('#LoginFormPresenter-email')
      .type('zarko.stepanov10@gmail.com')
      .should('have.value', 'zarko.stepanov10@gmail.com')
    cy.get('#LoginFormPresenter-password') 
      .type('Shubham##99')
    cy.get('#LoginFormPresenter-submit').click()  
    cy.get('#switchCompanyHeader')
      .contains('Switch Company')    
    cy.contains('Select a company below to sign-in to')
    //Click on company QA Zarko
    cy.get('button').contains('QA Zarko').click()
    //.wait(3000)

    //Dashboard
    cy.contains('Dashboard').should('have.attr', 'href','/').click()

    //Document List
    cy.contains('Document List').should('have.attr', 'href','/document_revision').click()

    //Change Request List
    cy.contains('Change Request List').should('have.attr', 'href','/change_request').click()
    
    //Change Request List
    cy.contains('User Management').should('have.attr', 'href','/user_management').click()

    //Group Management
    cy.contains('Group Management').should('have.attr', 'href','/group_management').click()
    
    //Receivable
    cy.contains('Receivable').click()
    .should('have.attr', 'href','/document_by_type/rc')
    //.wait(3000)

    //Purchace Order
    cy.contains('Purchase Order').click()
    .should('have.attr', 'href','/document_by_type/po')
    
    //Supplier
    cy.contains('Supplier').click()
    .should('have.attr', 'href','/document_by_type/sup')
    
    })    

})