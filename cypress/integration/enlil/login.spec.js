/// <reference types="cypress" />

context('Enlil', () => {
    beforeEach(() => {
      cy.visit('https://demo.enlil.io/login')
    })

    //Assert text elements and text on login form
    it('Assert elements on login page' ,() => {
        cy.get('img').then(($el) => {
            Cypress.dom.isVisible($el) // true
        })

        cy.document().should('have.property', 'charset').and('eq','UTF-8')
        cy.get('#root').should('be.visible')    
        //Wellcome text Sign in to Enlil  
        cy.get('#LoginFormPresenter-title').contains('Sign in to Enlil')
        cy.contains('h4','Sign in to Enlil')
        //EMAIL ADDRESS
        cy.contains('EMAIL ADDRESS')
        cy.get('[name="email"]').and('be.enabled').and('be.visible')
        //Link tesxt Forgot your password?
        cy.get('#LoginFormPresenter-forgotLink').contains('Forgot your password?')
        //PASSWORD
        cy.contains('PASSWORD')
        cy.get('[name="password"]').and('be.enabled').and('be.visible')
        //Link text Click here to register
        cy.contains("Click here to register")
          .should('have.attr', 'href','/signup')
    })   
    //Fail to login #1
    it('Fail to login - Required', () => {
      //Click button SIgn in 
      cy.get('#LoginFormPresenter-submit').click()       
      cy.screenshot()
      //Alert message is present 
      cy.get('#field-email > .MuiFormHelperText-root').contains('Required').and('be.visible')
      cy.get('#field-password > .MuiFormHelperText-root').contains('Required').and('be.visible')
      })

    //Fail to login #2
    it('Fail to login - Incorrect username or password', () => {
        //Type fake email into Email addres input field
        cy.get('#LoginFormPresenter-email')
          .type('fake@email.com').should('have.value', 'fake@email.com')
        //Type password into password input field 
        cy.get('#LoginFormPresenter-password') 
          .type('Tester1')
        //Click button SIgn in 
        cy.get('#LoginFormPresenter-submit').click() 
        //Alert message is present - Incorrect username or password.
        cy.get('#LoginFormPresenter-formError').screenshot()
          .contains('Incorrect username or password.')
        cy.screenshot()
        })

    //Login with success
    it('Login with success', () => {
        //Type email into Email addres input field
        cy.get('#LoginFormPresenter-email')
          .type('zarko.stepanov10@gmail.com')
          .should('have.value', 'zarko.stepanov10@gmail.com')
        //Type password into password input field    
        cy.get('#LoginFormPresenter-password') 
          .type('Shubham##99')
        //Click button Sign in
        cy.get('#LoginFormPresenter-submit').click()  
        cy.screenshot()
        //Comany page should be present
        cy.get('#switchCompanyHeader')
          .contains('Switch Company')    
        cy.contains('Select a company below to sign-in to')
        //Present Companies
        cy.get('button').contains('Giraffe Company')
        cy.get('button').contains('QA Zarko')
        cy.get('button').contains('Test_Company')
        //Click button for QA Zarko Company
        cy.get('button').contains('QA Zarko').click()
        cy.screenshot()
        })    
    })