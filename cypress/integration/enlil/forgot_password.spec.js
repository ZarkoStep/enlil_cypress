/// <reference types="cypress" />

context('Enlil', () => {
    beforeEach(() => {
      cy.visit('https://demo.enlil.io')
    })

    //Assert text elements and text on login form
    it('Forgot your password?' ,() => {
        //Click link text Forgot your password?
        cy.get('#LoginFormPresenter-forgotLink').contains('Forgot your password?').click()
        cy.get('img').then(($el) => {
            Cypress.dom.isVisible($el) // true
        })
        //Text assertation
        cy.contains('h4','Forgot your password?')
        cy.contains('h6','Enter yout email address below and we will send you a secure link to reset your password')
        cy.contains('h6','EMAIL ADDRESS').and('be.visible')
        cy.get('#ForgotPasswordPage-email').and('be.enabled').and('be.visible')
        //Click link reset my password
        cy.get('#ForgotPasswordPage-submitNewPassword').contains('Reset my Password').click()
        cy.screenshot()
        //Alert message is present - email must be an email
        cy.get('#ForgotPasswordPage-formError').contains('email must be an email').and('be.visible')
        //Alert message is present - Required
        cy.contains('p','Required').and('be.visible')
        //Click button Close 
        cy.get('#ForgotPasswordPage-linkRoute').contains('CLOSE')
        .click()    
    })
    //Fail to reset password not an existing user
    it('Fail to reset password not an existing user' ,() => {
        //Click link text Forgot your password?
        cy.get('#LoginFormPresenter-forgotLink').contains('Forgot your password?')
        .and('be.visible').click()
        //Type fake email in input field email
        cy.get('[id="ForgotPasswordPage-email"]')
        .type('fake@email.com').should('have.value', 'fake@email.com')   
        //Click link text Reset my Password         
        cy.get('#ForgotPasswordPage-submitNewPassword').contains('Reset my Password')
        .and('be.visible').click()
        cy.screenshot()
        //Alert message is present - Incorrect username or password.
        cy.contains('Incorrect username or password.').and('be.visible')
        //Click button Close
        cy.get('#ForgotPasswordPage-linkRoute').contains('CLOSE').click()
        cy.screenshot()
    })  

    //Fail to reset password invalid password
    it('Fail to reset password invalid password' ,() => {
        //Click link text Forgot your password?
        cy.get('#LoginFormPresenter-forgotLink').contains('Forgot your password?').click()
        cy.screenshot()
        //Type invalid email in input field email
        cy.get('[id="ForgotPasswordPage-email"]')
        .type('zarko.stepanov10').should('have.value', 'zarko.stepanov10')            
        cy.get('#ForgotPasswordPage-submitNewPassword').contains('Reset my Password').click()
        cy.screenshot()
        //Alert message is present - email must be an email
        cy.contains('email must be an email').and('be.visible')
        //Alert message is present - Invalid email address
        cy.contains('Invalid email address').and('be.visible')
        //Click button Close
        cy.get('#ForgotPasswordPage-linkRoute').contains('CLOSE').click()
        cy.screenshot()
    })  

    //Reset password with success
    it('Reset password with success' ,() => {
        //Click link text Forgot your password?
        cy.get('#LoginFormPresenter-forgotLink').contains('Forgot your password?').click()
        //Type email in input field email
        cy.get('[id="ForgotPasswordPage-email"]')
        .type('zarko.stepanov10@gmail.com').should('have.value', 'zarko.stepanov10@gmail.com')    
        //Click button Reset my Password      
        cy.get('#ForgotPasswordPage-submitNewPassword').contains('Reset my Password').click()
        cy.screenshot()
        //Notice message is visble
        cy.contains('We have sent an email to').and('be.visible')
        cy.contains('"zarko.stepanov10@gmail.com"').and('be.visible')
        cy.contains("Click the secure link we sent you to reset your password. If you didn't receive an email, check your spam folder.").and('be.visible')
        cy.screenshot()
    })   
    
})
